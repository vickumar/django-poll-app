django-poll-app
===============

The solution to https://docs.djangoproject.com/en/dev/intro/tutorial01/

Getting Started
---------------

### initial setup ###
1. Install requirements: `pip install -r requirements.txt`
2. Migrate the database: `python manage.py migrate`
3. Run the server: ``python manage.py runserver``
4. Open website in browser at ``http://localhost:8000/polls`` or admin at ``http://localhost:8000/admin`` (admin:admin)